﻿using PuMpkController.Views;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace PuMpkController.Services
{
    /// <summary>
    /// Навигатор по страницам приложения
    /// </summary>
    public class PagesNavigator : IPagesNavigator
    {
        /// <summary>
        /// Перейти к странице управления устройством
        /// </summary>
        /// <returns></returns>
        public async Task GoControlDevicePage()
        {
            await App.Current.MainPage.Navigation.PushModalAsync(new NavigationPage(new ControlDevicePage()));
        }

        /// <summary>
        /// Вернуться на предыдущую страницу
        /// </summary>
        /// <returns></returns>
        public async Task GoPrevPage()
        {
            await App.Current.MainPage.Navigation.PopModalAsync();
        }

        /// <summary>
        /// Перейти на главную страницу
        /// </summary>
        public void GoMainPage()
        {            
            App.Current.MainPage = new MainPage();            
        }

        /// <summary>
        /// Перейти на страницу выбора устройства
        /// </summary>
        /// <returns></returns>
        public async Task GoPuMpkSelectionPage()
        {
            await App.Current.MainPage.Navigation.PushModalAsync(new NavigationPage(new SelectedDevicePage()));
        }

        /// <summary>
        /// Перейти на страницу "О программе"
        /// </summary>
        /// <returns></returns>
        public async Task GoAboutPage()
        {
            await App.Current.MainPage.Navigation.PushModalAsync(new NavigationPage(new AboutPage()));
        }
    }
}
