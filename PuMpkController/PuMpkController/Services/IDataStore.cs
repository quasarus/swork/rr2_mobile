﻿using PuMpkController.Models;
using PuMpkController.ViewModels;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace PuMpkController.Services
{
    public interface IDataStore
    {
        void SetSelectedBluetoothDevice(BtDev device);
        BtDev GetSelectedBluetoothDevice();
        void SetSelectedBluetoothDeviceType(CustomBtDeviceType customBtDeviceType);
        
    }
}
