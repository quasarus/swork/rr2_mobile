﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Android;
using Android.App;
using Android.Bluetooth;
using Android.Content;
using Android.Content.PM;
using Android.OS;
using Android.Runtime;
using Android.Support.Design.Widget;
using Android.Support.V4.App;
using Android.Support.V4.Content;
using Android.Views;

using Plugin.BLE;
using Plugin.BLE.Abstractions.Contracts;
using Plugin.BLE.Abstractions.EventArgs;
using PuMpkController.DependencyServices;
using PuMpkController.Droid.Specific;


[assembly: Xamarin.Forms.Dependency(typeof(DependencyServiceHandler))]
namespace PuMpkController.Droid.Specific
{
    

    /// <summary>
    /// Реализация операций, специфичных для Android OS (служба зависимостей)
    /// </summary>
    public class DependencyServiceHandler: IDepServices
    {           
        /// <summary>
        /// Включение Bluetooth
        /// </summary>
        /// <returns></returns>
        public Task EnableBluetooth()
        {
            return Task.Factory.StartNew(() =>
            {
                //менеджер Bluetooth
                BluetoothManager btManager = (BluetoothManager)Application.Context.GetSystemService(Context.BluetoothService);
                //включение Bluetooth
                btManager.Adapter.Enable();               
                
            });           
            
        }

        /// <summary>
        /// Является ли устройство BLE
        /// </summary>
        /// <param name="device"></param>
        /// <returns></returns>
        public bool IsBleDevice(IDevice device)
        {
            try
            {
                if (device.GetType() != typeof(Plugin.BLE.Android.Device))
                {
                    return false;
                }
                var bleDev = (Plugin.BLE.Android.Device)device;
                return bleDev.BluetoothDevice.Type == BluetoothDeviceType.Le ? true : false;
            }
            catch (Exception)
            {
                return false;
            }
                        
        }



    }
}