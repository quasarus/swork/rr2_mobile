﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Text;
using Xamarin.Forms;

namespace PuMpkController.Converters
{
    /// <summary>
    /// Преобразование значения Rssi устройства Bluetooth в цвет контрола
    /// </summary>
    public class RssiToColorConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            int rssi = (int)value;                                  //получение значения Rssi из привязанного св-ва
            Color color = Color.Gray;                               //цыет по умолчанию
            //установка цвета, в зависимости от того, в какой диапазон попадает значение Rssi
            if (rssi == int.MaxValue)
            {
                color = Color.Gray;
            }
            else if (rssi > -30)
            {
                color = Color.FromHex("#001a00");

            }
            else if (rssi <= -30 && rssi > -67)
            {
                color = Color.FromHex("#004d00");

            }
            else if (rssi <= -67 && rssi > -70)
            {
                color = Color.FromHex("#99cc00");

            }
            else if (rssi <= -70 && rssi > -80)
            {
                color = Color.FromHex("ffcc00");

            }
            else if (rssi <= -80 && rssi > -90)
            {
                color = Color.FromHex("ff6600");

            }
            else if (rssi <= -90)
            {
                color = Color.FromHex("#cc0000");
            }
            return color;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
