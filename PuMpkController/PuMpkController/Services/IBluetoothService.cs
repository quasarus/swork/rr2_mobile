﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Text;
using System.Threading.Tasks;
using Plugin.BLE.Abstractions.Contracts;
using PuMpkController.Models;

namespace PuMpkController.Services
{
    
    public interface IBluetoothService
    {
        void StopSearchBluetoothDevicesAsync();
        void StartSearchBluetoothDevicesAsync(Action<BtDev> devDiscCallback, Action<bool> scanCallback);
        
        Task<bool> StartConnectToSelectedDevice(BtDev savedSelDevice, IAdapter adapter);
        Task DisconnectDevice(BtDev savedSelDevice);
        void StopConnectToSelectedDevice();
        Task<bool> StartExecuteDeviceCommand(DevCommand command, BtDev selectedDevice);
        void StopExecuteDeviceCommand();
        void Dispose();

        
        
    }
}
