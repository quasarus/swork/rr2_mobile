﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace PuMpkController.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class SelectedDevicePage : ContentPage
    {
        public SelectedDevicePage()
        {
            InitializeComponent();
            BindingContext = App.Locator.SelectedDevPageVm;
        }
    }
}