﻿using GalaSoft.MvvmLight.Ioc;
using PuMpkController.Models;
using PuMpkController.Services;
using System;
using System.Collections.Generic;
using System.Text;

namespace PuMpkController.ViewModels
{
    /// <summary>
    /// Локатор моделей-представления
    /// </summary>
    public class ViewModelLocator
    {
        static ViewModelLocator()
        {
            //Регистрация моделей-представления, сервисов, хранилищ
            
            SimpleIoc.Default.Register<MainPageVm>(); 
            SimpleIoc.Default.Register<SelectedDevicePageVm>();
            SimpleIoc.Default.Register<ControlDevicePageVm>();
            SimpleIoc.Default.Register<MenuPageVm>();
            SimpleIoc.Default.Register<AboutViewModel>();
            SimpleIoc.Default.Register<IBluetoothService,BluetoothService> ();
            SimpleIoc.Default.Register<IDataStore, DataStorage>();
            SimpleIoc.Default.Register<IPagesNavigator, PagesNavigator>();
        }

        public MainPageVm Main
        {
            get
            {
                return SimpleIoc.Default.GetInstance<MainPageVm>();                
            }
        }

        

        public SelectedDevicePageVm SelectedDevPageVm
        {
            get
            {
                return SimpleIoc.Default.GetInstance<SelectedDevicePageVm>();
            }
        }

        public ControlDevicePageVm ControlDevPageVm
        {
            get
            {
                return SimpleIoc.Default.GetInstance<ControlDevicePageVm>();
            }
        }

        public MenuPageVm MenuPageVm
        {
            get
            {
                return SimpleIoc.Default.GetInstance<MenuPageVm>();
            }
        }

        public AboutViewModel AboutViewModel
        {
            get
            {
                return SimpleIoc.Default.GetInstance<AboutViewModel>();
            }
        }

        public void OnAppSleep()
        {
            SimpleIoc.Default.GetInstance<MainPageVm>()?.OnAppSleep();
        }
    }
}
