﻿using System;
using System.Windows.Input;

using Xamarin.Forms;

namespace PuMpkController.ViewModels
{
    /// <summary>
    /// Модель-представление для страницы "О программе"
    /// </summary>
    public class AboutViewModel : BaseViewModel
    {
        private string _aboutText;
        /// <summary>
        /// Основной текст страницы
        /// </summary>
        public string AboutText
        {
            get { return _aboutText; }
            set
            {
                _aboutText = value;
                OnPropertyChanged(nameof(AboutText));
            }
        }



        public AboutViewModel()
        {
            Title = "Сведения о программе";
            AboutText = "Заглушка. Заполнить текстом.";
        }

        
    }
}