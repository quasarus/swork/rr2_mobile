﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PuMpkController.Models
{

    public enum MenuItemType
    {
        About        
    }

    //Пункт меню главной страницы приложения
    public class HomeMenuItem
    {
        /// <summary>
        /// Тип пункта
        /// </summary>
        public MenuItemType Id { get; set; }
        /// <summary>
        /// Заголовок
        /// </summary>
        public string Title { get; set; }
        
    }
}
