﻿using Plugin.BLE.Abstractions.Contracts;
using Plugin.BLE.Abstractions.EventArgs;
using PuMpkController.Models;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Timers;

namespace PuMpkController.Services
{
    /// <summary>
    /// Часть службы, реализующей отправку команды по Bluetooth
    /// </summary>
    public partial class BluetoothService : IBluetoothService, IDisposable
    {
        /// <summary>
        /// Источник токена отмены задачи отправки команды AT
        /// </summary>
        private CancellationTokenSource СtsCommand;
        /// <summary>
        /// Таймаут ожидания подтверждения на команду
        /// </summary>
        private const int CommandAckTimeoutMs = 4000;
        /// <summary>
        /// Начало отправки команды
        /// </summary>
        /// <param name="command">Команда</param>
        /// <param name="device">Устройство</param>
        /// <returns></returns>
        public async Task<bool> StartExecuteDeviceCommand(DevCommand command, BtDev device)
        {
            //Характеристика службы, эмулирующей UART
            ICharacteristic characteristic = device.Characteristic;

            try
            {
                //получение источника токена
                СtsCommand = GetCts(СtsCommand);

                //Запись байтов команды
                bool isWriteOk = await characteristic.WriteAsync(
                    Encoding.ASCII.GetBytes(command.AtReq), СtsCommand.Token);
                //проверка что задача не была отменена
                СtsCommand.Token.ThrowIfCancellationRequested();

                //проверить успешность записи
                if (!isWriteOk)
                {
                    throw new Exception("");
                }

            }
            catch (OperationCanceledException)
            {
                //при отмене операции разорвать соединение
                await DisconnectDevice(device);
            }
            catch (Exception)
            {
                await DisconnectDevice(device);
                await App.Current.MainPage.DisplayAlert("Ошибка",
                    $"Не удалось записать данные через службу Bluetooth" +
                    $" устройства {device.Description}", "Ок");
                return false;
            }

            //байты сообщения подтверждения
            byte[] ack = null;
            //флаг того, что произошло обновление значения
            bool isUpdate = false;
            //инициализация обработчика события обновления значения характеристики
            EventHandler<CharacteristicUpdatedEventArgs> charUpdH = new EventHandler<CharacteristicUpdatedEventArgs>((o, e)
                  =>
            {
                isUpdate = true;
                СtsCommand?.Cancel();
                ack = e.Characteristic.Value;                   //сохранение обновленного значения
            });
            //подписка на событие обновления значения характеристики
            characteristic.ValueUpdated += charUpdH;

            await characteristic.StartUpdatesAsync();           //старт ожидания обновления значения характеристики

            //старт задачи ожидания подтверждения
            var timeoutTask = Task.Delay(CommandAckTimeoutMs, СtsCommand.Token);
            await Task.WhenAny(timeoutTask);
            //по завершению задачи остановить слежение за обновлением характеристики, отписаться от события
            await characteristic.StopUpdatesAsync();
            characteristic.ValueUpdated -= charUpdH;

            //если задача ожидания была отменена и не произошло обновления, то считать, что ее отменил пользователь
            if (timeoutTask.IsCanceled)
            {
                if (!isUpdate)
                {
                    await DisconnectDevice(device);
                    return false;
                }
            }
            else
            {
                //если задача не была отменена - был таймаут
                await HandleDevCommandError($"Не получен ответ от {device.Description}",
                    device);
                return false;
            }
            //проверка значения полученного подтверждения
            if (ack == null || ack.Length == 0)
            {
                await HandleDevCommandError($"Ошибка при чтении значения ответа от {device.Description}",
                    device);
                return false;
            }            

            //если значение подтверждения не равно ожидаемому - вернуть ошибку
            if (Encoding.ASCII.GetString(ack) != command.AtAck)
            {
                await HandleDevCommandError($"Неверное подтверждение получения команды от {device.Description}",
                    device);

                return false;
            }
            return true;
        }

        /// <summary>
        /// Останов выполнения команды
        /// </summary>
        public void StopExecuteDeviceCommand()
        {
            //остановить задачу
            СtsCommand?.Cancel();
            //разрушить объект устройства, с которым было сопряжение
            _dataStore.GetSelectedBluetoothDevice().Dispose();
        }

        /// <summary>
        /// Обработка ошибки отправки команды
        /// </summary>
        /// <param name="v">Описание ошибки</param>
        /// <param name="selectedDevice">Устройство</param>
        /// <returns></returns>
        private async Task HandleDevCommandError(string v, BtDev selectedDevice)
        {
            await DisconnectDevice(selectedDevice);
            await App.Current.MainPage.DisplayAlert("Ошибка", v, "Ok");
        }
    }
}
