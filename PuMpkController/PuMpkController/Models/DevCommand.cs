﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PuMpkController.Models
{
    /// <summary>
    /// Команды для устройства по Bluetooth
    /// </summary>
    public class DevCommand
    {
        /// <summary>
        /// Описание
        /// </summary>
        public string Description { get; set; }
        /// <summary>
        /// Имя
        /// </summary>
        public string Name { get; set; }
        /// <summary>
        /// AT- команда запроса
        /// </summary>
        public string AtReq { get; set; }
        /// <summary>
        /// AT- команда подтверждения
        /// </summary>
        public string AtAck { get; set; }
    }
}
