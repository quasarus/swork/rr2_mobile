﻿using Plugin.BLE;
using Plugin.BLE.Abstractions;
using Plugin.BLE.Abstractions.Contracts;
using Plugin.BLE.Abstractions.EventArgs;
using PuMpkController.DependencyServices;
using PuMpkController.Models;
using System;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace PuMpkController.Services
{
    /// <summary>
    /// Часть службы, реализующей общий функционал работы с Bluetooth
    /// </summary>
    public partial class BluetoothService : IBluetoothService, IDisposable
    {
        private IDataStore _dataStore;                              //хранилище данных
        private IPagesNavigator _navigator;                         //навигатор по страницам

        private Guid ServiceGuid { get; set; }                      //GUID службы, эмулирующей UART
        private Guid CharacteristicGuid { get; set; }               //GUID характеристики службы, эмулирующей UART


        private IAdapter adapter = null;                            //адаптер BLE
        private const int BleScanTime = 30000;                      //время сканирования устройств BLE в мс

        /// <summary>
        /// Инициализация службы
        /// </summary>
        /// <param name="dataStore"></param>
        /// <param name="navigator"></param>
        public BluetoothService(IDataStore dataStore, IPagesNavigator navigator)
        {
            //инициализация интерфейсов IOC
            _dataStore = dataStore;
            _navigator = navigator;
            //инициализация GUID службы , эмулирующей UART, а также GUID ее характеристики 
            ServiceGuid = Guid.ParseExact("0000ffe0-0000-1000-8000-00805f9b34fb", "d");
            CharacteristicGuid = Guid.ParseExact("0000ffe1-0000-1000-8000-00805f9b34fb", "d");            
            
        }
        
        /// <summary>
        /// Источник токена отмены задачи сканирования устройств BLE
        /// </summary>
        private CancellationTokenSource ScanCts;
        /// <summary>
        /// Запуск сканирования устройств BLE
        /// </summary>
        /// <param name="devCallback">Обратный вызов по нахождению устройства BLE</param>
        /// <param name="scanStartEndCallback">Обратный вызов по началу/завершению сканирования</param>
        public async void StartSearchBluetoothDevicesAsync(Action<BtDev> devCallback,Action<bool> scanStartEndCallback)
        {            
            try
            {               
                //проверка того, что Bluetooth включен
                if (!await CheckBluetoothOn())
                {
                    return;
                }               

            }
            catch (Exception ex)
            {
                await App.Current.MainPage.DisplayAlert("Внимание",
                        "Не удалось включить Bluettoth", "Ok");
                return;

            }
            
            //инициализация обработчика по событию обнаружения нового устройства
            EventHandler<DeviceEventArgs> devH = new EventHandler<DeviceEventArgs>((o,e)=>
            {
                //Проверка того, что устройство - BLE
                bool isBle = DependencyService.Get<IDepServices>().IsBleDevice(e.Device);
                
                if (devCallback != null && isBle)       //если BLE - инициализировать новое устройство
                {
                    BtDev newDev = new BtDev()
                    {
                        Name = e.Device.Name,
                        Rssi = e.Device.Rssi,
                        State = e.Device.State,
                        Id = e.Device.Id,                        
                        Instance = e.Device
                    };

                    devCallback(newDev);                //сделать обратный вызов
                }
            });

            adapter.ScanTimeout = BleScanTime;          //установка времени сканирования
            adapter.DeviceDiscovered += devH;           //подписка на событие обнаружение нового устройства

            ScanCts = GetCts(ScanCts);                  //получить источника токена задачи сканирования

            scanStartEndCallback?.Invoke(true);         //обратный вызов по началу задачи сканирования
            //запуск сканирования
            await adapter.StartScanningForDevicesAsync(null,null,true, ScanCts.Token);
            
            scanStartEndCallback?.Invoke(false);        //обратный вызов по завершению задачи сканирования            
            adapter.DeviceDiscovered -= devH;           //отписка от события обнаружения нового устройства BLE
        }

        /// <summary>
        /// Останов задачи сканирования новых устройств BLE
        /// </summary>
        public void StopSearchBluetoothDevicesAsync()
        {            
            ScanCts?.Cancel();                                   
        }

        /// <summary>
        /// Проверка включения Bluetooth
        /// </summary>
        /// <returns></returns>
        private async Task<bool> CheckBluetoothOn()
        {

            adapter = CrossBluetoothLE.Current.Adapter;                     //адаптер BLE
            if (adapter == null)                                            //проверка адаптера BLE
            {
                return false;
            }

            if (!CrossBluetoothLE.Current.IsOn)                             //если Bluetooth выключен
            {
                //запрос к пользователю на разрешение включения Bluettoth
                var answer = await App.Current.MainPage.DisplayAlert("Внимание",
                    "Включить Bluetooth для поиска устройств?", "Да", "Нет");
                //проверка подтверждения пользователем
                if (!answer)
                {
                    return false;
                }

                //источник токена на задачу ожидания включения Bluettoth
                CancellationTokenSource ctsWait =  new CancellationTokenSource();
                //инициализация обработчика события изменения состояния адаптера Bluetooth
                EventHandler<BluetoothStateChangedArgs> btChangedEvh =
                    new EventHandler<BluetoothStateChangedArgs>((o, e) =>
                    {
                        //если Bluettoth включился - отменить токен ожидания включения Bluettoth
                        if (e.NewState == BluetoothState.On)
                        {
                            ctsWait.Cancel();
                        }
                    });
                //подписка на событие изменения состояния адаптера Bluetooth
                CrossBluetoothLE.Current.StateChanged += btChangedEvh;

                //Bluetooth (для каждой платформы - свое)
                await DependencyService.Get<IDepServices>().EnableBluetooth();
                int bluetoothOnWaitTimeout = 0;
                //установить таймаут ожидания вкл. Bluetooth в зависимости от ОС
                if (Device.RuntimePlatform == Device.iOS)
                {
                    //для iOS время включения больше, т.к. включает пользователь в настройках
                    bluetoothOnWaitTimeout = 30000;
                }
                else if (Device.RuntimePlatform == Device.Android)
                {
                    bluetoothOnWaitTimeout = 6000;
                }

                //задача ожидания включения Bluetooth
                var timeoutTask = Task.Delay(bluetoothOnWaitTimeout, ctsWait.Token);
                
                await Task.WhenAny(timeoutTask);                            //Ожидание завершения задачи
                ctsWait.Dispose();                                          //разрушение источника токена задачи ожидания
                if (!timeoutTask.IsCanceled)                                //если задача не была отменена, то считать, что был таймаут - вывести сообщение
                {                    
                    await App.Current.MainPage.DisplayAlert("Внимание",
                    "Не удалось включить Bluetooth.", "Ok");
                    return false;
                }
                //отписка от события изменения состояния адаптера Bluetooth
                CrossBluetoothLE.Current.StateChanged -= btChangedEvh;           

            }
            return true;
        }
                
        /// <summary>
        /// Источник токена для задачи соединения с устройством Bluetooth
        /// </summary>
        private CancellationTokenSource СtsConnect;
        /// <summary>
        /// Обработчик события потери соединения с устройством Bluetooth
        /// </summary>
        private EventHandler<DeviceErrorEventArgs> devConnectionLostHandler;
        /// <summary>
        /// Обработчик события потери разрыва соединения устройством Bluetooth
        /// </summary>
        private EventHandler<DeviceEventArgs> devDisconnectedHandler;
        /// <summary>
        /// Запуск соединения с устройством BLE
        /// </summary>
        /// <param name="selDevice">Выбранное пользователем устройство BLE</param>
        /// <param name="adapter">Адаптер BLE</param>
        /// <returns></returns>
        public async Task<bool> StartConnectToSelectedDevice(BtDev selDevice, IAdapter adapter)
        {
            //Проверить, не подключено ли уже устройство
            if (selDevice.State == DeviceState.Connected)
            {
                return true;
            }
            
            IDevice device = selDevice.Instance;                    //библиотечный класс устройства BLE

            СtsConnect = GetCts(СtsConnect);                        //получения источника токена
                        
            try
            {
                //асинхронное подключение к устройству
                await adapter.ConnectToDeviceAsync(selDevice.Instance, new ConnectParameters(), СtsConnect.Token);
                //проверить, не была ли отменена задача подключения 
                СtsConnect.Token.ThrowIfCancellationRequested();
                //бросить исключение, если сопряжения с устройством нет
                if (device.State != DeviceState.Connected)
                {
                    throw new Exception("");
                }

                //инициализация обработчиков событий разрыва соединения или его потери
                devConnectionLostHandler = new EventHandler<DeviceErrorEventArgs>((o, e) =>
                { OnDeviceConnectionLostOrDisconnected(); });
                devDisconnectedHandler = new EventHandler<DeviceEventArgs>((o, e) =>
                { OnDeviceConnectionLostOrDisconnected(); });

                //подписка на события разрыва соединения или его потери
                adapter.DeviceConnectionLost += devConnectionLostHandler;
                adapter.DeviceDisconnected += devDisconnectedHandler;                

                //получение характеристики службы устройства
                ICharacteristic characteristic = await GetDeviceCharacteristic(selDevice, СtsConnect.Token);
                //проверить, не была ли отменена задача подключения 
                СtsConnect.Token.ThrowIfCancellationRequested();
                //проверка полученной характеристики
                if (characteristic == null)
                {
                    await DisconnectDevice(selDevice);
                    throw new Exception("Устройство не является ПУ или МПК.");
                }
                selDevice.Characteristic = characteristic;
            }
            catch (OperationCanceledException)
            {
                await DisconnectDevice(selDevice);
                return false;
            }
            catch (Exception ex)
            {
                HandleConnectError(ex.Message);
                return false;
            }
            return true;
        }

        /// <summary>
        /// Получение источника токена для задачи
        /// </summary>
        /// <param name="cts">Исходный источник токена</param>
        /// <returns></returns>
        private CancellationTokenSource GetCts(CancellationTokenSource cts)
        {
            //если исходный источник токена не инициализирован - инициализировать
            if (cts == null)
            {
                return new CancellationTokenSource();
            }
            else
            {
                //проверить, был ли отменен источник токена
                if (cts.IsCancellationRequested)
                {
                    //если отменен - разрушить и инициализировать новый
                    cts.Dispose();
                    return new CancellationTokenSource(); ;
                }
            }
            return cts;
        }

        /// <summary>
        /// По событию потери связи с устройством или разрыву соединения устройством
        /// </summary>
        private void OnDeviceConnectionLostOrDisconnected()
        {
            //выбранное устройство, с которым производилось соединение
            BtDev device = _dataStore.GetSelectedBluetoothDevice();
            //установить состояние устройства
            device.State = DeviceState.Disconnected;
            UnsubscribeConnectionLostAndDisconnected();
            
            //Вызвать сообщение о потери связи в основном потоке GUI
            Device.BeginInvokeOnMainThread(async () =>
            {
                await App.Current.MainPage.DisplayAlert("Внимание",
                $"Потеряна связь с устройством {device.Description}.", "Ok");
            });
            //вернуться на главную страницу
            _navigator.GoMainPage();
        }

        /// <summary>
        /// Отписка от событий потери связи с устройством или разрыву соединения устройством
        /// </summary>
        private void UnsubscribeConnectionLostAndDisconnected()
        {
            if (devDisconnectedHandler != null)
            {
                CrossBluetoothLE.Current.Adapter.DeviceDisconnected -= devDisconnectedHandler;
            }

            if (devConnectionLostHandler != null)
            {
                CrossBluetoothLE.Current.Adapter.DeviceConnectionLost -= devConnectionLostHandler;
            }
        }

        /// <summary>
        /// Получение характеристики службы устройства
        /// </summary>
        /// <param name="device">Устройство</param>
        /// <param name="token">Источник токена отмены</param>
        /// <returns></returns>
        private async Task<ICharacteristic> GetDeviceCharacteristic(BtDev device, CancellationToken token)
        {
            //проверить, не отменен ли токен
            token.ThrowIfCancellationRequested();
            //получение службы, эмулирующей UART
            IService service = await device.Instance.GetServiceAsync(ServiceGuid);
            //проверить, не отменен ли токен
            token.ThrowIfCancellationRequested();
            //проверка службы
            if (service == null)
            {
                await DisconnectDevice(device);
                return null;
            }
            //получение характеристики
            ICharacteristic characteristic = await service.GetCharacteristicAsync(CharacteristicGuid);
            //проверить, не отменен ли токен
            token.ThrowIfCancellationRequested();
            return characteristic == null ? null : characteristic;            
        }

        /// <summary>
        /// Отмена задачи соединения с устройством
        /// </summary>
        public void StopConnectToSelectedDevice()
        {
            СtsConnect?.Cancel();
        }

        /// <summary>
        /// Обработать ошибку соединения
        /// </summary>
        /// <param name="message"></param>
        private async void HandleConnectError(string message)
        {
            await App.Current.MainPage.DisplayAlert("Ошибка.",
                    $"Ошибка соединения с устройством по Bluetooth. {message}", "Ок");
        }        

        /// <summary>
        /// Разорвать связь с устройством
        /// </summary>
        /// <param name="device">Устройство</param>
        /// <returns></returns>
        public async Task DisconnectDevice(BtDev device)
        {

            UnsubscribeConnectionLostAndDisconnected();
            //проверка, что сопряжение с устройством еще есть
            if (device.Instance.State == DeviceState.Connected)
            {
                await CrossBluetoothLE.Current.Adapter.DisconnectDeviceAsync(device.Instance);
            }        
            //установка состояния
            device.State = DeviceState.Disconnected;
        }       

        

        #region IDisposable Support
        private bool disposedValue = false; // To detect redundant calls

        protected virtual void Dispose(bool disposing)
        {
            if (!disposedValue)
            {
                if (disposing)
                {
                    // TODO: dispose managed state (managed objects).
                }
                //отмена токенов
                ScanCts?.Cancel();
                СtsCommand?.Cancel();
                СtsConnect?.Cancel();

                UnsubscribeConnectionLostAndDisconnected();                            

                disposedValue = true;
            }
        }

        // TODO: override a finalizer only if Dispose(bool disposing) above has code to free unmanaged resources.
        // ~BluetoothService() {
        //   // Do not change this code. Put cleanup code in Dispose(bool disposing) above.
        //   Dispose(false);
        // }

        // This code added to correctly implement the disposable pattern.
        public void Dispose()
        {
            // Do not change this code. Put cleanup code in Dispose(bool disposing) above.
            Dispose(true);
            // TODO: uncomment the following line if the finalizer is overridden above.
            // GC.SuppressFinalize(this);
        }
        #endregion
    }
}
