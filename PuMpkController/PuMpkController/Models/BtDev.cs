﻿using GalaSoft.MvvmLight;
using Plugin.BLE;
using Plugin.BLE.Abstractions;
using Plugin.BLE.Abstractions.Contracts;
using PuMpkController.ViewModels;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace PuMpkController.Models
{
    
    /// <summary>
    /// Модель Bluetooth устройства
    /// </summary>
    public class BtDev: ObservableObject,IDisposable
    {
        //Интервал обновления значения Rssi при подключенном устройстве
        private const int RssiUpdateIntervalMs = 5000;

        /// <summary>
        /// Имя устройства
        /// </summary>
        public string Name { get; set; }
        /// <summary>
        /// Описание типа устройства
        /// </summary>
        public string Description { get; set; }        
        /// <summary>
        /// Внутренний идентификатор (порядковый номер, согласно последовательности обнаружения устройства)
        /// </summary>
        public int InternalId { get; set; }
        
        /// <summary>
        /// GUID устройства
        /// </summary>
        public Guid Id { get; set; }        
        
        

        /// <summary>
        /// GATT характеристика
        /// </summary>
        public ICharacteristic Characteristic { get; set; }       


        private int _rssi;
        /// <summary>
        /// Значение RSSI 
        /// </summary>
        public int Rssi
        {
            get { return _rssi; }
            set
            {
                _rssi = value;
                RaisePropertyChanged(nameof(Rssi));
            }
        }


        private bool _isUpdateRssi;
        /// <summary>
        /// Флаг, указывающий, обновлять ли RSSI
        /// </summary>
        private bool IsUpdateRssi
        {
            get { return _isUpdateRssi; }
            set
            {
                _isUpdateRssi = value;
                HandleUpdateRssiFlag(IsUpdateRssi);
            }
        }

        private IDevice _instance;
        /// <summary>
        /// Ссылка на внутренний объект устройства библиотеки Plugin.BLE
        /// </summary>
        public IDevice Instance
        {
            get { return _instance; }
            set
            {
                _instance = value;                
            }
        }


        
        private DeviceState _devState;
        /// <summary>
        /// Состояние устройства
        /// </summary>
        public DeviceState State
        {
            get { return _devState; }
            set
            {
                _devState = value;
                //заполнение описания состояния устройства
                switch (State)
                {
                    case DeviceState.Disconnected:
                        StateDescription = "Отключено";
                        IsUpdateRssi = false;
                        break;
                    case DeviceState.Connecting:
                        StateDescription = "Сопряжение";
                        break;
                    case DeviceState.Connected:
                        IsUpdateRssi = true;
                        StateDescription = "Сопряжено";
                        break;
                    case DeviceState.Limited:
                        StateDescription = "Ограничено";
                        break;
                    default:
                        break;
                }
            }
        }

        /// <summary>
        /// Описание состояния устройства
        /// </summary>
        public string StateDescription { get; set; }

        private CustomBtDeviceType _devType;
        /// <summary>
        /// Тип устройства
        /// </summary>
        public CustomBtDeviceType DevType
        {
            get { return _devType; }
            set
            {
                _devType = value;
                //заполнение описания типа устройства
                switch (value)
                {
                    case CustomBtDeviceType.PU:
                        Description = "ПУ";
                        break;
                    case CustomBtDeviceType.MPK:
                        Description = "МПК";
                        break;
                    default:
                        break;
                }
            }
        }

        
        /// <summary>
        /// Источник отмены токена для задачи запроса Rssi устройства
        /// </summary>
        private CancellationTokenSource RssiCts; 
        /// <summary>
        /// Обработка изменения флага обновления RSSI устройства
        /// </summary>
        /// <param name="isUpdate"></param>
        private async void HandleUpdateRssiFlag(bool isUpdate)
        {
            ClearCts(RssiCts);                                           //очистка источника токена
            //если выставлен флаг старта обновления RSSI - Запустить задачу обновления RSSI
            if (isUpdate)
            {
                RssiCts = new CancellationTokenSource();                //инициализация источника токена
                await UpdateRssiAsync(RssiCts);                         //ожидание обновления RSSI
            }
            

        }

        /// <summary>
        /// Задача обновления RSSI
        /// </summary>
        /// <param name="rssiCts"></param>
        /// <returns></returns>
        private async Task UpdateRssiAsync(CancellationTokenSource rssiCts)
        {
            //Пока не будет отменен источник токена, производить обновление RSSI через интервал времени
            while (true)
            {
                try
                {
                    await Instance.UpdateRssiAsync();                           //обновить RSSI с устройства                   
                    Rssi = Instance.Rssi;                                       //установить значение RSSI в классе устройства
                    await Task.Delay(RssiUpdateIntervalMs, rssiCts.Token);      //выдерживание интервала
                    rssiCts.Token.ThrowIfCancellationRequested();               //проверка токна на отмену
                }
                catch (Exception)                                               //по исключению или отмене токена выйти из задачи обновления RSSI
                {
                    break;
                }
            }
        }
        
        public BtDev()
        {
            Rssi = int.MaxValue;
        }

        //очистка источника токена
        private void ClearCts(CancellationTokenSource rssiCts)
        {
            //проверка токена на null
            if (rssiCts != null)
            {
                
                if (!rssiCts.IsCancellationRequested)           //если токен еще не был отменен - отменить
                {
                    rssiCts.Cancel();
                }
                rssiCts.Dispose();                              //разрушить источник токена                   
            }
        }

        #region Реализация интерфейса IDisposable
        private bool disposedValue = false; // To detect redundant calls

        protected async virtual void Dispose(bool disposing)
        {
            if (!disposedValue)
            {
                if (disposing)
                {
                    // TODO: dispose managed state (managed objects).
                }
                //выключить обновление RSSI
                IsUpdateRssi = false;
                //если устройство еще сопряжено - разорвать сопряжение
                if (State == DeviceState.Connected)
                {
                    await CrossBluetoothLE.Current.Adapter.DisconnectDeviceAsync(Instance);
                }
                // TODO: free unmanaged resources (unmanaged objects) and override a finalizer below.
                // TODO: set large fields to null.

                disposedValue = true;
            }
        }

        // TODO: override a finalizer only if Dispose(bool disposing) above has code to free unmanaged resources.
        // ~BtDev() {
        //   // Do not change this code. Put cleanup code in Dispose(bool disposing) above.
        //   Dispose(false);
        // }

        // This code added to correctly implement the disposable pattern.
        public void Dispose()
        {
            // Do not change this code. Put cleanup code in Dispose(bool disposing) above.
            Dispose(true);
            // TODO: uncomment the following line if the finalizer is overridden above.
            // GC.SuppressFinalize(this);
        }
        #endregion


    }
}
