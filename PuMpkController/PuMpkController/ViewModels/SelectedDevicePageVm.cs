﻿using GalaSoft.MvvmLight.Command;
using Plugin.BLE;
using Plugin.BLE.Abstractions;
using Plugin.BLE.Abstractions.Contracts;

using PuMpkController.Models;
using PuMpkController.Services;
using PuMpkController.Views;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace PuMpkController.ViewModels
{
    /// <summary>
    /// Проектный тип устройства
    /// </summary>
    public enum CustomBtDeviceType
    {
        PU,         //ПУ
        MPK         //МПК
    }

    /// <summary>
    /// Модель-представления страницы выбора проектного типа BLE устройства
    /// </summary>
    public class SelectedDevicePageVm : BaseViewModel
    {
        private ObservableCollection<BtDev> _devicesForSelection;
        /// <summary>
        /// Список устройств для выбора
        /// </summary>
        public ObservableCollection<BtDev> DevicesForSelection
        {
            get { return _devicesForSelection; }
            set
            {
                _devicesForSelection = value;
                OnPropertyChanged(nameof(DevicesForSelection));
            }
        }

        private BtDev _selCustomDev;
        /// <summary>
        /// Выбранное устройство
        /// </summary>
        public BtDev SelectedCustomDevice
        {
            get { return _selCustomDev; }
            set
            {
                _selCustomDev = value;                
                OnPropertyChanged(nameof(SelectedCustomDevice));
                OnPuMpkDeviceSelect(SelectedCustomDevice);
                if (SelectedCustomDevice != null)
                {
                    SelectedCustomDevice = null;
                }
            }
        }

        private bool isConnecting;
        //Флаг - есть ли соединение с выбранным устройством
        public bool IsConnecting
        {
            get { return isConnecting; }
            set
            {
                isConnecting = value;
                OnPropertyChanged(nameof(IsConnecting));
            }
        }



        private string _topText;
        //Текст с описанием страницы
        public string TopText
        {
            get { return _topText; }
            set
            {
                _topText = value;
                OnPropertyChanged(nameof(TopText));
            }
        }



        //команды по исчезновению и появлению страницы
        public RelayCommand AppearingCommand { get; }
        public RelayCommand DisappearingCommand { get; }

        //IOC
        private IBluetoothService _btService;
        private IDataStore _dataStore;
        private IPagesNavigator _pNavigator;

        /// <summary>
        /// Инициализация модели-представления
        /// </summary>
        /// <param name="dataStore"></param>
        /// <param name="pNavigator"></param>
        /// <param name="btService"></param>
        public SelectedDevicePageVm(IDataStore dataStore, IPagesNavigator pNavigator, IBluetoothService btService)
        {
            _btService = btService;
            _dataStore = dataStore;
            _pNavigator = pNavigator;
            //инициализация команд
            AppearingCommand = new RelayCommand(OnPageAppearing);
            DisappearingCommand = new RelayCommand(OnPageDisappearing);
            //инициализация списка проектных устр-в для выбора
            DevicesForSelection = new ObservableCollection<BtDev>();
            DevicesForSelection.Add(new BtDev()
            {
                DevType = CustomBtDeviceType.PU
            });
            DevicesForSelection.Add(new BtDev()
            {
                DevType = CustomBtDeviceType.MPK
            });
            Title = "Выбор устройства";
        }

        /// <summary>
        /// При пропадании страницы - остановить соединение с устройством
        /// </summary>
        private void OnPageDisappearing()
        {
            _btService.StopConnectToSelectedDevice();
        }

        /// <summary>
        /// При выборе устройства МПК-ПУ
        /// </summary>
        /// <param name="selDevice"></param>
        private async void OnPuMpkDeviceSelect(BtDev selDevice)
        {
            if (SelectedCustomDevice != null)
            {
                //сохранить тип устройства
                _dataStore.SetSelectedBluetoothDeviceType(selDevice.DevType);
                SelectedCustomDevice = null;    
                
                BtDev savedSelDevice = _dataStore.GetSelectedBluetoothDevice();

                //начать подключение с выбранным устройством
                IsConnecting = true;
                bool isConnected = await _btService.StartConnectToSelectedDevice(savedSelDevice, CrossBluetoothLE.Current.Adapter) ;
                IsConnecting = false;
                //проверка успешности подключения
                if (!isConnected)
                {
                    //установить состояние, вернуться на главную страницу
                    savedSelDevice.State = DeviceState.Disconnected;
                    _pNavigator.GoMainPage();
                }
                else
                {
                    //сохранить выбранное устройство, перейти на страницу управления им
                    _dataStore.SetSelectedBluetoothDevice(savedSelDevice);
                    savedSelDevice.State = DeviceState.Connected;                    
                    await _pNavigator.GoControlDevicePage();
                }                

            }
        }        

        /// <summary>
        /// При появлении страницы
        /// </summary>
        private  void OnPageAppearing()
        {
            // остановить поиск BLE устройст, инициированный на предыдущей странице
            _btService.StopSearchBluetoothDevicesAsync();
        }
    }
}
