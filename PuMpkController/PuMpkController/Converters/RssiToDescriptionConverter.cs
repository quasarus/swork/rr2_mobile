﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Text;
using Xamarin.Forms;

namespace PuMpkController.Converters
{
    /// <summary>
    /// Преобразование значения Rssi устройства Bluetooth в комментарий к значению
    /// </summary>
    public class RssiToDescriptionConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            int rssi = (int)value;                              //получение значения Rssi
            string description = "";
            string param = (string)parameter;                   //Получение параметра
            //получение комментария, в зависимости от того, в какой диапазон попадает значение Rssi
            if (rssi == int.MaxValue)
            {
                description = $"{param}Неизвестно";
            }
            else if (rssi > -30)
            {
                description = $"{param}Отличный";

            }
            else if (rssi <= -30 && rssi > -67)
            {
                description = $"{param}Очень хороший";

            }
            else if (rssi <= -67 && rssi > -70)
            {
                description = $"{param}Хороший";

            }
            else if (rssi <= -70 && rssi > -80)
            {
                description = $"{param}Плохой";

            }
            else if (rssi <= -80 && rssi > -90)
            {
                description = $"{param}Очень плохой";

            }
            else if (rssi <= -90)
            {
                description = $"{param}Нет связи";
            }
            return description;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
