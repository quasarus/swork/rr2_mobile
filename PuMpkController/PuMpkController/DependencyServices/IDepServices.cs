﻿using Plugin.BLE.Abstractions.Contracts;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace PuMpkController.DependencyServices
{
    /// <summary>
    /// Методы службы зависимости для взаимодействия со специфическими API для каждой платформы iOS,Android
    /// </summary>
    public interface IDepServices
    {
        /// <summary>
        /// Включение Bluetooth
        /// </summary>
        /// <returns></returns>
        Task EnableBluetooth();
        /// <summary>
        /// Проверка, является ли устройство BLE
        /// </summary>
        /// <param name="device"></param>
        /// <returns></returns>
        bool IsBleDevice(IDevice device);


    }
}
