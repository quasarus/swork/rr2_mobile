﻿using System;
using System.Linq;
using Android.App;
using Android.Content.PM;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.OS;
using PuMpkController.Droid.Specific;
using System.Collections.Generic;
using Android;
using System.Threading.Tasks;
using Android.Support.V4.Content;
using Android.Support.V4.App;

namespace PuMpkController.Droid
{
    [Activity(Label = "PuMpkController", Icon = "@mipmap/icon", Theme = "@style/MainTheme", MainLauncher = true, ConfigurationChanges = ConfigChanges.ScreenSize | ConfigChanges.Orientation)]
    public class MainActivity : global::Xamarin.Forms.Platform.Android.FormsAppCompatActivity
    {
        //Список необходимых разрешений для работы приложения
        private readonly List<string> RequiredPermissions = new List<string>()
        {
            Manifest.Permission.Bluetooth,
            Manifest.Permission.BluetoothAdmin,
            Manifest.Permission.AccessCoarseLocation,
            Manifest.Permission.AccessFineLocation            
        };
        //код запроса
        private const int ReqCode = 0;

        /// <summary>
        /// Переопределение обработчика создания основной активности
        /// </summary>
        /// <param name="savedInstanceState"></param>
        protected override void OnCreate(Bundle savedInstanceState)
        {            
            
            TabLayoutResource = Resource.Layout.Tabbar;
            ToolbarResource = Resource.Layout.Toolbar;

            base.OnCreate(savedInstanceState);
            global::Xamarin.Forms.Forms.Init(this, savedInstanceState);

            Xamarin.Forms.DependencyService.Register<DependencyServiceHandler>();

            //загрузить приложение           
            LoadApplication(new App());
            //попытаться проверить доступ к разрешениям и получить, если разрешения не выданы
            TryGetPermissions();

        }

        /// <summary>
        /// Вызов метода, когда пользователь разрешил/отклонил запрос на разрешения для приложения
        /// </summary>
        /// <param name="requestCode">Код запроса</param>
        /// <param name="permissions">Массив запрашиваемых разрешений</param>
        /// <param name="grantResults">Список разрешений</param>
        public override void OnRequestPermissionsResult(int requestCode, string[] permissions, [GeneratedEnum] Permission[] grantResults)
        {
            
            if (requestCode == ReqCode)
            {
                //если одно из запрошенных разрешений не выданы, вывести сообщение и закрыть программу
                if (grantResults.Any(n => n != (int)Permission.Granted))
                {
                    ShowAlert("Внимание!", "Пользователь не предоставил права приложению " +
                        "и оно будет закрыто", () => { this.FinishAffinity(); });                    
                }                
                
            }
            base.OnRequestPermissionsResult(requestCode, permissions, grantResults);
        }


        private void TryGetPermissions()
        {
            //получить список невыданных разрешений
            List<string> notAllowedPermissions = GetNotAllowedPermissions();
            
            //если все разрешения уже есть - вернуться
            if (notAllowedPermissions.Count == 0)
            {
                return;
            }

            /*если версия SDK < 23, то разрешения должны были быть выданы с помощью манифеста, а если какие-то разрешения не предоставлены,
             то пользователь сам их изменил - выдать ему сообщение чтоб он их выдал вручную. 
             Если SDK > 23, то необходимо отправить запрос пользователю на разрешения*/
            if ((int)Build.VERSION.SdkInt < 23)
            {
                ShowAlert("Внимание!", "Приложению не даны необходимые разрешения. Задайте разрешения для работы с местоположением и Bluetooth для" +
                    " приложения в настройках системы.", () => { this.FinishAffinity(); });
                
                
            }
            else
            {
                //запрос разрешений
                ActivityCompat.RequestPermissions(this, notAllowedPermissions.ToArray(), ReqCode);
            }           
            
        }

        /// <summary>
        /// Получение списка невыданных разрешений
        /// </summary>
        /// <returns></returns>
        private List<string> GetNotAllowedPermissions()
        {
            return RequiredPermissions.Where(y =>
            ContextCompat.CheckSelfPermission(Application.Context, y) != (int)Permission.Granted)
            .ToList();
        }

        /// <summary>
        /// Показать предупреждение
        /// </summary>
        /// <param name="title"></param>
        /// <param name="text"></param>
        /// <param name="onButtonPress"></param>
        private void ShowAlert(string title, string text, Action onButtonPress)
        {
            AlertDialog.Builder dialog = new AlertDialog.Builder(this);
            AlertDialog alert = dialog.Create();
            alert.SetTitle(title);
            alert.SetMessage(text);
            alert.SetIcon(Resource.Drawable.mr_button_dark);
            alert.SetButton("OK", (c, ev) =>
            {
                onButtonPress?.Invoke();
            });            
            alert.Show();
        }
    }
}