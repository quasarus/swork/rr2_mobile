﻿using GalaSoft.MvvmLight.Command;

using PuMpkController.Models;
using PuMpkController.Services;
using PuMpkController.Views;
using System;
using System.Linq;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Text;
using Xamarin.Forms;


namespace PuMpkController.ViewModels
{
    /// <summary>
    /// Модель-представления для главной страницы
    /// </summary>
    public class MainPageVm : BaseViewModel
    {
        private ObservableCollection<BtDev> _foundedDevs;
        /// <summary>
        /// Обнаруженные устройства BLE
        /// </summary>
        public ObservableCollection<BtDev> FoundedDevs
        {
            get { return _foundedDevs; }
            set
            {
                SetProperty(ref _foundedDevs, value);
            }
        }

        private BtDev _selDevice;
        /// <summary>
        /// Выбранное устройство BLE
        /// </summary>
        public BtDev SelectedDevice
        {
            get { return _selDevice; }
            set
            {
                _selDevice = value;
                OnPropertyChanged(nameof(SelectedDevice));
                OnItemSelected();
                if (SelectedDevice != null)
                {
                    SelectedDevice = null;
                }
            }
        }

        private bool _isRefreshing;
        /// <summary>
        /// Флаг процедуры обновления списка устройств BLE
        /// </summary>
        public bool IsRefreshing
        {
            get { return _isRefreshing; }
            set
            {
                _isRefreshing = value;                
                OnPropertyChanged(nameof(IsRefreshing));
            }
        }

        private bool _isShowHint;
        /// <summary>
        /// Флаг отображения подсказки
        /// </summary>
        public bool IsShowHint
        {
            get { return _isShowHint; }
            set
            {
                _isShowHint = value;
                if (IsShowHint)
                {
                    FoundedDevs.Clear();
                }
                OnPropertyChanged(nameof(IsShowHint));
            }
        }

        private bool _isScanning;
        /// <summary>
        /// Флаг хода процедуры сканирования
        /// </summary>
        public bool IsScanning
        {
            get { return _isScanning; }
            set
            {
                _isScanning = value;
                OnPropertyChanged(nameof(IsScanning));
            }
        }

        
        public RelayCommand AppearingCommand { get; }       //команда появления страницы
        public RelayCommand DisappearingCommand { get; }    //команда исчезновения страницы
        public RelayCommand RefreshCommand { get; }         //команда обновления списка устройств BLE

        //IOC
        private IBluetoothService _btService;
        private IDataStore _dataStore;
        private IPagesNavigator _pNavigator;
        /// <summary>
        /// Инициализация модели-представления
        /// </summary>
        /// <param name="btService"></param>
        /// <param name="dataStore"></param>
        /// <param name="pNavigator"></param>
        public MainPageVm(IBluetoothService btService, IDataStore dataStore, IPagesNavigator pNavigator)
        {
            _pNavigator = pNavigator;
            _btService = btService;
            _dataStore = dataStore;
            //инициализация команд
            AppearingCommand = new RelayCommand(OnMainPageAppearing);
            DisappearingCommand = new RelayCommand(OnMainPageDisappearing);              
            RefreshCommand = new RelayCommand(OnDevListStartRefresh);

            FoundedDevs = new ObservableCollection<BtDev>();                     
            Title = "Bluetooth устройства";
        }

        /// <summary>
        /// По появлению страницы
        /// </summary>
        private void OnMainPageAppearing()
        {
            //показывать подсказку
            IsShowHint = true;
        }


        /// <summary>
        /// При исчезновении страницы
        /// </summary>
        private void OnMainPageDisappearing()
        {                       
                        
        }

        /// <summary>
        /// При обновлении списка Bluetooth устройств
        /// </summary>
        private void OnDevListStartRefresh()
        {
            IsRefreshing = false;
            _btService.StopSearchBluetoothDevicesAsync();
            StartMainWork();
        }

        /// <summary>
        /// По уходу приложения в сон
        /// </summary>
        public void OnAppSleep()
        {           
            
            
        }

        /// <summary>
        /// При выборе пользователем устройства Bluetooth
        /// </summary>
        private async void OnItemSelected()
        {
            if (SelectedDevice != null)
            {
                //выбор из списка устр-ва по внутреннему ID
                var selDev = FoundedDevs.SingleOrDefault(f => f.InternalId == SelectedDevice.InternalId);
                var notSelectedDevices = FoundedDevs.Where(f => f.InternalId != SelectedDevice.InternalId);
                //сохранение выбранного устройства
                _dataStore.SetSelectedBluetoothDevice(selDev);               
                //обнуление выбранного устройства (для корректного отображения в GUI)
                SelectedDevice = null;
                //переход к странице выбора МПК-ПУ
                await _pNavigator.GoPuMpkSelectionPage();
            }
            
        }        


              

        /// <summary>
        /// Старт основной работы приложения
        /// </summary>
        private void StartMainWork()
        {
            IsShowHint = false;
            _btService.Dispose();
            
            FoundedDevs.Clear();
            
            //IsRefreshing = true;            
            _btService.StartSearchBluetoothDevicesAsync(DeviceFoundedAction,
                (isScan) =>
                {
                    IsScanning = isScan;
                });
        }

        /// <summary>
        /// При появлении нового устройства
        /// </summary>
        /// <param name="dev">Новое устройство</param>
        private void DeviceFoundedAction(BtDev dev)
        {
            //снять флаг обновления
            IsRefreshing = false;
            //получить внутренний ID нового устройства
            dev.InternalId = FoundedDevs.Count > 0 ? FoundedDevs.Max(c => c.InternalId) + 1 : 1;
            //добавить в список
            FoundedDevs.Add(dev);
        }
    }
}
