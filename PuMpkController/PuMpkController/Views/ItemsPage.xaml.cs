﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

using PuMpkController.Models;
using PuMpkController.Views;
using PuMpkController.ViewModels;

namespace PuMpkController.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class ItemsPage : ContentPage
    {
        //ItemsViewModel viewModel;

        public ItemsPage()
        {
            InitializeComponent();
            BindingContext = App.Locator.Main;            
        }

        

    }
}