﻿using System;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using PuMpkController.Views;
using PuMpkController.ViewModels;

[assembly: XamlCompilation(XamlCompilationOptions.Compile)]
namespace PuMpkController
{
    /// <summary>
    /// Текущее приложение
    /// </summary>
    public partial class App : Application
    {

        private static ViewModelLocator _locator;
        /// <summary>
        /// Локатор моделей-представления
        /// </summary>
        public static ViewModelLocator Locator
        {
            get
            {
                return _locator ?? (_locator = new ViewModelLocator());
            }
        }




        public App()
        {
            InitializeComponent();  
            //инициализация главной страницы
            MainPage = new MainPage();
        }

        

        protected override void OnStart()
        {
            
        }

        protected override void OnSleep()
        {
            Locator.OnAppSleep();
        }

        protected override void OnResume()
        {
            // Handle when your app resumes
        }
    }
}
