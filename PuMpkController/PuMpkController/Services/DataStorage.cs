﻿using System;
using System.Collections.Generic;
using System.Text;
using PuMpkController.Models;
using PuMpkController.ViewModels;

namespace PuMpkController.Services
{
    /// <summary>
    /// Хранилище данных приложения
    /// </summary>
    public class DataStorage : IDataStore
    {
        /// <summary>
        /// Выбранное пользователем устройство для сопряжения
        /// </summary>
        private BtDev _selectedBtDev { get; set; }     

        /// <summary>
        /// Получение выбранного устройства
        /// </summary>
        /// <returns></returns>
        public BtDev GetSelectedBluetoothDevice()
        {
            return _selectedBtDev;
        }

        /// <summary>
        /// Установка выбранного устройства
        /// </summary>
        /// <param name="device"></param>
        public void SetSelectedBluetoothDevice(BtDev device)
        {
            _selectedBtDev = device;
        }

        /// <summary>
        /// Установка типа выбранного устройства
        /// </summary>
        /// <param name="customBtDeviceType"></param>
        public void SetSelectedBluetoothDeviceType(CustomBtDeviceType customBtDeviceType)
        {
            _selectedBtDev.DevType = customBtDeviceType;
        }

        
    }
}
