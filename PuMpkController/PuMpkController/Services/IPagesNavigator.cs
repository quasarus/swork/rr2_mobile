﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace PuMpkController.Services
{
    public interface IPagesNavigator
    {
        Task GoPrevPage();
        Task GoPuMpkSelectionPage();
        void GoMainPage();
        Task GoControlDevicePage();
        Task GoAboutPage();
    }
}
