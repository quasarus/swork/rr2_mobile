﻿using PuMpkController.Models;
using PuMpkController.Services;
using System;
using System.Collections.Generic;
using System.Text;

namespace PuMpkController.ViewModels
{
    /// <summary>
    /// Модель-представления меню основной страницы
    /// </summary>
    public class MenuPageVm : BaseViewModel
    {
        private IPagesNavigator _pagesNavigator;

        /// <summary>
        /// Элементы меню
        /// </summary>
        public List<HomeMenuItem> MenuItems { get; set; }

        private HomeMenuItem _selectedItem;
        /// <summary>
        /// Выбранный элемент меню
        /// </summary>
        public HomeMenuItem SelectedMenuItem
        {
            get { return _selectedItem; }
            set
            {
                _selectedItem = value;
                OnPropertyChanged(nameof(SelectedMenuItem));
                if (SelectedMenuItem != null)
                {
                    OnMenuItemSelection(SelectedMenuItem);
                    SelectedMenuItem = null;
                }
            }
        }

        private async void OnMenuItemSelection(HomeMenuItem selectedMenuItem)
        {
            //При выборе элемента "О приложении", перейти на соответствующую страницу
            switch (selectedMenuItem.Id)
            {
                case MenuItemType.About:
                    await _pagesNavigator.GoAboutPage();
                    break;
                default:
                    break;
            }
        }

        /// <summary>
        /// Инициализация модели-представления
        /// </summary>
        /// <param name="pagesNavigator"></param>
        public MenuPageVm(IPagesNavigator pagesNavigator)
        {
            _pagesNavigator = pagesNavigator;
            //заполнение списка эл-тов меню
            MenuItems = new List<HomeMenuItem>
            {
                new HomeMenuItem {Id = MenuItemType.About, Title="О программе" }                
            };
        }
    }
}
