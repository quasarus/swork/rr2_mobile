﻿using GalaSoft.MvvmLight.Command;

using PuMpkController.Models;
using System;
using System.Collections.Generic;
using System.Text;
using Xamarin.Forms;
using System.Linq;
using PuMpkController.Services;

namespace PuMpkController.ViewModels
{
    /// <summary>
    /// Модель представления страницы управления устройством
    /// </summary>
    public class ControlDevicePageVm : BaseViewModel
    {

        private readonly List<DevCommand> DevCommands;                   //список команд для устройства

        //IOC ссылки
        private IDataStore _dataStore;
        private IBluetoothService _btService;
        private IPagesNavigator _navigator;

        public RelayCommand<string> DeviceCommand { get; }              //команда нажатия на кнопку команды
        public RelayCommand AppearingCommand { get; }                   //команда появления страницы
        public RelayCommand DisappearingCommand { get; }                //команда исчезновения страницы
        private string _topText;
        /// <summary>
        /// Текст вверху страницы с ее описанием
        /// </summary>
        public string TopText
        {
            get { return _topText; }
            set
            {
                _topText = value;
                OnPropertyChanged(nameof(TopText));
            }
        }

        private bool _isWaitAnswer;
        /// <summary>
        /// Флаг ожидания ответа подтверждения на команду
        /// </summary>
        public bool IsWaitAnswer
        {
            get { return _isWaitAnswer; }
            set
            {
                _isWaitAnswer = value;
                OnPropertyChanged(nameof(IsWaitAnswer));
            }
        }

        private BtDev _selectedDevice;
        /// <summary>
        /// Выбранное устройство, на которое отправляется команда
        /// </summary>
        public BtDev SelectedDevice
        {
            get { return _selectedDevice; }
            set
            {
                _selectedDevice = value;
                OnPropertyChanged(nameof(SelectedDevice));
            }
        }


        /// <summary>
        /// Инициализация модели-представления
        /// </summary>
        /// <param name="dataStore"></param>
        /// <param name="btService"></param>
        /// <param name="navigator"></param>
        public ControlDevicePageVm(IDataStore dataStore, IBluetoothService btService,
            IPagesNavigator navigator)
        {
            _dataStore = dataStore;
            _btService = btService;
            _navigator = navigator;
            //инициализация комманд
            DeviceCommand = new RelayCommand<string>(OnUserDeviceCommand);
            AppearingCommand = new RelayCommand(OnPageAppearing);
            DisappearingCommand = new RelayCommand(OnPageDisappearing);

            Title = "Управление";                                               //установка заголовка страницы

            //инициализация списка команд
            DevCommands = new List<DevCommand>();
            #region Инициализация списка команд
            DevCommands.Add(new DevCommand()
            {
                Name="+90",
                Description="Поворот в положение +90 градусов",
                AtReq = "AT+POS90",
                AtAck="OK"
            });
            DevCommands.Add(new DevCommand()
            {
                Name = "-90",
                Description = "Поворот в положение -90 градусов",
                AtReq = "AT+NEG90",
                AtAck = "OK"
            });
            DevCommands.Add(new DevCommand()
            {
                Name = "0",
                Description = "Поворот в положение 0 градусов",
                AtReq = "AT+ZERO",
                AtAck = "OK"
            });
            DevCommands.Add(new DevCommand()
            {
                Name = "Захват",
                Description = "Захват объекта",
                AtReq = "AT+CATCH",
                AtAck = "OK"
            });
            DevCommands.Add(new DevCommand()
            {
                Name = "Освобождение",
                Description = "Освобождение объекта",
                AtReq = "AT+FREE",
                AtAck = "OK"
            });
            #endregion
        }

        /// <summary>
        /// При исчезновении страницы страницы
        /// </summary>
        private void OnPageDisappearing()
        {
            //Остановить выполнение команды и вернуться на главную страницу
            _btService.StopExecuteDeviceCommand();
            _navigator.GoMainPage();
        }

        /// <summary>
        /// При появлении страницы
        /// </summary>
        private void OnPageAppearing()
        {
            //в зависимости от выбранного устройства поменять описание страницы
            SelectedDevice = _dataStore.GetSelectedBluetoothDevice();
            switch (SelectedDevice.DevType)
            {
                case CustomBtDeviceType.PU:
                    TopText = "Команды для исполнения поворотным устройством";
                    break;
                case CustomBtDeviceType.MPK:
                    TopText = "Команды для записи в модуль передачи команд";
                    break;
                default:
                    break;
            }
        }

        /// <summary>
        /// При отправке команды
        /// </summary>
        /// <param name="commandParam"></param>
        private async void OnUserDeviceCommand(string commandParam)
        {
            //выбрать команду из списка
            var command = DevCommands.SingleOrDefault(g => g.Name == commandParam);
            if (command == null)
            {
                return;
            }

            IsWaitAnswer = true;        //установить флаг ожидания подтверждения на команду
            //выполнить команду
            bool isCommandExecuted = await _btService.StartExecuteDeviceCommand(command, SelectedDevice);
            IsWaitAnswer = false;       //снять флаг ожидания подтверждения на команду
            //если команда не выполнена - перейти на главную
            if (!isCommandExecuted)
            {                
                _navigator.GoMainPage();
            }
        }
    }
}
