﻿using SkiaSharp;
using SkiaSharp.Views.Forms;
using System;
using System.Collections.Generic;
using System.Text;
using Xamarin.Forms;

namespace PuMpkController.Views
{
    public class PuCommandButton : Button
    {
        
        public PuCommandButton()
        {
            //this.PaintSurface += PuCommandButton_PaintSurface;            
        }

        private void PuCommandButton_PaintSurface(object sender, SKPaintSurfaceEventArgs e)
        {
            
            // получаем текущую поверхность из аргументов
            var surface = e.Surface;
            // Получаем холст на котором будет рисовать
            var canvas = surface.Canvas;
            
            // Очищаем холст
            canvas.Clear(SKColors.White);

            var pathStroke = new SKPaint
            {
                IsAntialias = true,
                Style = SKPaintStyle.Stroke,
                Color = SKColors.Green,
                StrokeWidth = 5
            };

            var path = new SKPath();

            path.MoveTo(160, 60);
            path.LineTo(240, 140);
            path.MoveTo(240, 60);
            path.LineTo(160, 140);

            // Рисуем путь
            canvas.DrawPath(path, pathStroke);
        }

        /*public static readonly BindableProperty IsVerticalCustom =
    BindableProperty.Create("IsVerticalCustom", typeof(bool), typeof(Button),
                default(bool), propertyChanged: OnTextPropertyChanged);

        

        public bool IsVerticalCustomProp
        {
            get { return (bool)GetValue(IsVerticalCustom); }
            set { SetValue(IsVerticalCustom, value); }
        }

        private static void OnTextPropertyChanged(BindableObject bindable, object oldValue, object newValue)
        {
            bool oldVal = (bool)oldValue;
            bool newVal = (bool)newValue;
        }

        public PuCommandButton()
        {
            bool efe = IsVerticalCustomProp;
        }*/
    }
}
